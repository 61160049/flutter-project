import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class NoteForm extends StatefulWidget {
  String noteId;
  NoteForm({Key? key, required this.noteId}) : super(key: key);

  @override
  _NoteFormState createState() => _NoteFormState(this.noteId);
}

class _NoteFormState extends State<NoteForm> {
  String textTime = "Select date and time.";
  String userId = FirebaseAuth.instance.currentUser!.uid;
  String noteId;
  String noteTitle = "";
  String noteDetail = "";
  DateTime selectedDate = DateTime.now();
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  _NoteFormState(this.noteId);
  TextEditingController _noteTitleController = new TextEditingController();
  TextEditingController _noteDetailController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.noteId.isNotEmpty) {
      notes.doc(this.noteId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          noteTitle = data['note_title'];
          noteDetail = data['note_detail'];
          selectedDate = data['date'].toDate();
          textTime = this.selectedDate.toString();
          _noteTitleController.text = noteTitle;
          _noteDetailController.text = noteDetail;
          setState(() {
          });
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();
  Future<void> addNote() {
    return notes
        .add({
          'uid': this.userId,
          'date': this.selectedDate,
          'note_title': this.noteTitle,
          'note_detail': this.noteDetail,
        })
        .then((value) => print('Note Added'))
        .catchError((error) => print('Failed to add note: $error'));
  }

  Future<void> updateNote() {
    return notes
        .doc(this.noteId)
        .update({
          'date': this.selectedDate,
          'note_detail': this.noteDetail,
          'note_title': this.noteTitle,
        })
        .then((value) => print("Note Updated"))
        .catchError((error) => print("Failed to update note: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: Text('Add Note')),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: Column(
          children: [
            TextButton(
              onPressed: () {
                DatePicker.showDateTimePicker(
                  context,
                  showTitleActions: true,
                  onChanged: (date) {
                    print('change $date in time zone ' +
                        date.timeZoneOffset.inHours.toString());
                  },
                  onConfirm: (date) {
                    setState(() {
                      selectedDate = date;
                      textTime = selectedDate.toString();
                      print('confirm $textTime');
                    });
                  },
                  currentTime: selectedDate,
                  locale: LocaleType.en,
                );
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    Icons.calendar_today,
                    color: Colors.blue,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 8),
                    child: Text(
                      textTime,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    ),
                  )
                ],
              ),
            ),
            TextFormField(
              controller: _noteTitleController,
              decoration: InputDecoration(labelText: 'Note Title'),
              onChanged: (value) {
                setState(() {
                  noteTitle = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please input Title';
                }
                return null;
              },
            ),
            TextFormField(
              keyboardType: TextInputType.multiline,
              maxLines: 20,
              controller: _noteDetailController,
              onChanged: (value) {
                setState(() {
                  noteDetail = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please input Detail';
                }
                return null;
              },
            ),
            Expanded(
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: MaterialButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (noteId.isEmpty) {
                        await addNote();
                      } else {
                        await updateNote();
                      }
                      Navigator.pop(context);
                    }
                  },
                  child: Text('Save', style: TextStyle(fontSize: 20)),
                  color: Colors.blue.shade200,
                  textColor: Colors.black,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
