import 'dart:html';

import 'package:example_project/note_form.dart';
import 'package:example_project/note_information.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Memo Pad',
      theme: ThemeData(
        primaryColor: Colors.blue.shade200,
      ),
      home: MyHomePage(title: 'Memo Pad'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? _imageURL = null;
  String? _userName = null;
  void initState() {
    super.initState();
    setState(() {
      _imageURL = FirebaseAuth.instance.currentUser!.photoURL;
      _userName = FirebaseAuth.instance.currentUser!.displayName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: new Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.lightBlue,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _imageURL == null
                      ? const Icon(Icons.person)
                      : GestureDetector(
                          child: ClipOval(
                            child: Image.network(
                              _imageURL!,
                              width: 100,
                              height: 100,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                  SizedBox(
                    child: Text(''),
                  ),
                  _userName == null
                      ? Text('Unknown user')
                      : Center(
                          child: Text(
                            _userName!,
                            style: TextStyle(fontSize: 18, color: Colors.white),
                          ),
                        )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 70,right: 70),
              child: ElevatedButton(
                onPressed: () {
                  FirebaseAuth.instance.signOut();
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.logout),
                    SizedBox(child: Text(' '),),
                    Text(
                      'Sign Out',
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
                style: ElevatedButton.styleFrom(primary: Colors.red.shade300),
              ),
            )
          ],
        ),
      ),
      body: Center(child: NoteInformation()),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => NoteForm(noteId: '')));
        },
        tooltip: 'Add Note',
        child: Icon(Icons.add),
        backgroundColor: Colors.blue.shade200,
        hoverColor: Colors.blue,
        foregroundColor: Colors.black,
      ),
    );
  }
}
