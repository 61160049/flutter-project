import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/note_form.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class NoteInformation extends StatefulWidget {
  NoteInformation({Key? key}) : super(key: key);

  @override
  _NoteInformationState createState() => _NoteInformationState();
}

class _NoteInformationState extends State<NoteInformation> {
  final Stream<QuerySnapshot> _noteStream = FirebaseFirestore.instance
      .collection('notes')
      .where('uid',isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      .snapshots();
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  Future<void> delNote(noteId){
    return notes.doc(noteId).delete().then((value) => print('Note Deleted')).catchError((error) => print('Failed to delete note: $error'));
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _noteStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }

          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data()! as Map<String, dynamic>;
              return ListTile(
                title: Text(data['note_title'],style: TextStyle(fontSize: 20),),
                subtitle: Text(data['date'].toDate().toString(),style: TextStyle(fontSize: 15)),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => NoteForm(noteId: document.id)));
                },
                trailing: IconButton(icon: Icon(Icons.delete),onPressed: () async {
                  await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: new Text("Delete this note"),
                          content: new Text("Are you sure you want to delete?"),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text("Cancel"),
                              textColor: Colors.blue.shade200,
                              hoverColor: Colors.blue,
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            new FlatButton(
                              child: new Text("Delete"),
                              textColor: Colors.red,
                              onPressed: () {
                                delNote(document.id);
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                },),
              );
            }).toList(),
          );
        });
  }
}
