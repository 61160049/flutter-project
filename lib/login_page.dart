import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    return await FirebaseAuth.instance.signInWithPopup(googleProvider);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: new Text('Login'),
        backgroundColor: Colors.blue.shade200,
      ),
      body: ListView(
        padding: EdgeInsets.all(32),
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Text(
                  'Memo Pad',
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.all(5.0),
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              image: DecorationImage(
                  image: AssetImage('images/memo_pad.png'),
                  fit: BoxFit.fitHeight),
            ),
          ),
          SizedBox(
            child: Text(''),
          ),
          Container(
            padding: EdgeInsets.only(left: 300, right: 300),
            child: ElevatedButton(
              onPressed: () async {
                await signInWithGoogle();
              },
              child: Text(
                'Login with Google',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.lightGreen),
            ),
          ),
        ],
      ),
    );
  }
}
